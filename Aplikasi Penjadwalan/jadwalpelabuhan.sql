-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2020 at 10:32 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jadwalpelabuhan`
--

-- --------------------------------------------------------

--
-- Table structure for table `kapal`
--

CREATE TABLE `kapal` (
  `NamaKapal` varchar(20) NOT NULL,
  `Perusahaan` varchar(45) NOT NULL,
  `Muatan` int(11) NOT NULL,
  `Kategori` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kapal`
--

INSERT INTO `kapal` (`NamaKapal`, `Perusahaan`, `Muatan`, `Kategori`) VALUES
('ADINDA', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('ARIMBI', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('AUSTIN 01', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('AUSTIN 17', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('Doruli 02', 'PT. Aquafarm', 50, 'Tigaras'),
('Doruli 03', 'PT. Aquafarm', 50, 'Tigaras'),
('DOS ROHA I', 'OPS. Tomok Tour', 50, 'Tomok'),
('DOS ROHA II', 'OPS. Tomok Tour', 50, 'Tomok'),
('DOS ROHA III', 'OPS. Tomok Tour', 50, 'Tomok'),
('DOS ROHA V', 'OPS. Tomok Tour', 50, 'Tomok'),
('EKO 92', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('GLANTER', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('GLORIA', 'OPS. Tomok Tour', 50, 'Tomok'),
('Grace 3', 'PT. Gunung Hijau Megah', 50, 'Tigaras'),
('Holden 01', 'Putu Sumarjaya', 50, 'Tigaras'),
('Holden 02', 'Putu Sumarjaya', 50, 'Tigaras'),
('HORAS', 'OPS. Marihat Permai', 50, 'Tigaras'),
('Horas 01', 'PT.ASDP', 50, 'Tigaras'),
('Horas 02', 'PT.ASDP', 50, 'Tigaras'),
('KANRO', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('Lamhot', 'PT.Dok Bahari Nusantara', 50, 'Tigaras'),
('LEO START', 'OPS. Tomok Tour', 50, 'Tomok'),
('METHA', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('MURNI', 'OPS. Tomok Tour', 50, 'Tomok'),
('NATIO 1', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('PULO HORAS', 'OPS. Tomok Tour', 50, 'Tomok'),
('RODAME I', 'OPS. Tomok Tour', 50, 'Tomok'),
('RODAME II', 'OPS. Tomok Tour', 50, 'Tomok'),
('ROGANDA', 'OPS. Tomok Tour', 50, 'Tomok'),
('RUDI', 'OPS. Tomok Tour', 50, 'Tomok'),
('SARINA', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('SILIMA TALI', 'OPS. Tomok Tour', 50, 'Tomok'),
('SINAR TOBA 6', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('SINAR TOBA 7', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('Soluna 01', 'PT.ASDP', 50, 'Tigaras'),
('Soluna 02', 'PT.ASDP', 50, 'Tigaras'),
('STAR 03', 'OPS. Marihat Permai', 50, 'Tigaras'),
('STAR 04', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('TELAGA BIRU', 'OPS. Marihat Permai', 50, 'Onanrunggu'),
('TIO TOUR', 'OPS. Tomok Tour', 50, 'Tomok'),
('Yosuaris 01', 'PT.Dok Bahari Nusantara', 50, 'Tigaras'),
('Yosuaris 02', 'PT.Dok Bahari Nusantara', 50, 'Tigaras');

-- --------------------------------------------------------

--
-- Table structure for table `nahkoda`
--

CREATE TABLE `nahkoda` (
  `Inisial` char(2) NOT NULL,
  `NamaNahkoda` varchar(45) NOT NULL,
  `NamaKapal` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nahkoda`
--

INSERT INTO `nahkoda` (`Inisial`, `NamaNahkoda`, `NamaKapal`) VALUES
('Ad', 'Andika Samosir', 'Doruli 02'),
('ae', 'Arie Gultom', 'ARIMBI'),
('AG', 'Andi Gultom', 'Yosuaris 01'),
('bd', 'Budianto Sidauruk', 'STAR 04'),
('bm', 'Betman Malau', 'NATIO 1'),
('br', 'Budiman Rumahorbo', 'KANRO'),
('BS', 'Bornok Simbolon', 'Yosuaris 02'),
('BT', 'Bastian Tamba', 'DOS ROHA V'),
('cs', 'Charles Situngkir', 'TIO TOUR'),
('DD', 'Dedi Situngkir', 'Grace 3'),
('DN', 'Dohar Nadapdap', 'Horas 01'),
('DS', 'Danang Sidauruk', 'RODAME I'),
('fs', 'Febi Samosir', 'SARINA'),
('HS', 'Hasudungan Sihotang', 'DOS ROHA I'),
('JH', 'Joni Hutagaol', 'Doruli 03'),
('jk', 'Joko Simalango', 'GLANTER'),
('JN', 'Jojor Naibaho', 'Holden 02'),
('jo', 'Jojo Situmorang', 'MURNI'),
('Jr', 'Johan Simarmata', 'LEO START'),
('JS', 'Jecky Sitinjak', 'DOS ROHA III'),
('ju', 'Josua Simarmata', 'TELAGA BIRU'),
('kh', 'Krintianto Hutahaean', 'EKO 92'),
('KS', 'Kevin Sigalingging', 'DOS ROHA II'),
('Kt', 'kristian saragi', 'Lamhot'),
('Lk', 'Linggom Sitinjak', 'HORAS'),
('lm', 'Linggom Sitinjak', 'PULO HORAS'),
('LN', 'Lamhot Nainggolan', 'Holden 01'),
('MG', 'Martin Gultom', 'Soluna 01'),
('MN', 'Manahan Nainggolan', 'Horas 02'),
('MS', 'Mikael Simanjuntak', 'RODAME II'),
('mt', 'mangatur situmeang', 'SILIMA TALI'),
('pa', 'Putra Ambarita', 'AUSTIN 01'),
('pg', 'Pangihutan Simbolon', 'STAR 03'),
('pp', 'Pudan Parapat', 'SINAR TOBA 6'),
('ps', 'Pangihutan Simbolon', 'RUDI'),
('pu', 'Putra Ambarita', 'AUSTIN 17'),
('rl', 'Rico Lumbanraja', 'METHA'),
('rm', 'Raja Malau', 'ADINDA'),
('sp', 'Soon Parapat', 'SINAR TOBA 7'),
('SS', 'Septa Simalango', 'Soluna 02'),
('tg', 'Torang Gultom', 'ROGANDA'),
('YM', 'Yosef Malau', 'GLORIA');

-- --------------------------------------------------------

--
-- Table structure for table `pelabuhan`
--

CREATE TABLE `pelabuhan` (
  `NamaPelabuhan` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pelabuhan`
--

INSERT INTO `pelabuhan` (`NamaPelabuhan`) VALUES
('Ajibata'),
('Balige'),
('Nainggolan'),
('Onanrunggu'),
('Sibandang'),
('Simanindo'),
('Tigaras'),
('Tomok');

-- --------------------------------------------------------

--
-- Table structure for table `sesi`
--

CREATE TABLE `sesi` (
  `Sesi` int(11) NOT NULL,
  `Keberangkatan` int(11) NOT NULL,
  `NamaPelabuhan` varchar(45) NOT NULL,
  `NamaKapal` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wilayah`
--

CREATE TABLE `wilayah` (
  `Pelabuhan1` varchar(45) NOT NULL,
  `Pelabuhan2` varchar(45) NOT NULL,
  `Jarak` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wilayah`
--

INSERT INTO `wilayah` (`Pelabuhan1`, `Pelabuhan2`, `Jarak`) VALUES
('Ajibata', 'Tomok', 8.43),
('Ajibata', 'Onanrunggu', 23.76),
('Ajibata', 'Tigaras', 22.6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kapal`
--
ALTER TABLE `kapal`
  ADD PRIMARY KEY (`NamaKapal`),
  ADD UNIQUE KEY `NamaKapal_UNIQUE` (`NamaKapal`),
  ADD KEY `fk_Kapal_Pelabuhan_idx` (`Kategori`);

--
-- Indexes for table `nahkoda`
--
ALTER TABLE `nahkoda`
  ADD PRIMARY KEY (`Inisial`),
  ADD KEY `fk_Nahkoda_Kapal1_idx` (`NamaKapal`);

--
-- Indexes for table `pelabuhan`
--
ALTER TABLE `pelabuhan`
  ADD PRIMARY KEY (`NamaPelabuhan`),
  ADD UNIQUE KEY `idPelabuhan_UNIQUE` (`NamaPelabuhan`);

--
-- Indexes for table `sesi`
--
ALTER TABLE `sesi`
  ADD PRIMARY KEY (`Sesi`,`Keberangkatan`,`NamaPelabuhan`,`NamaKapal`),
  ADD KEY `fk_Sesi_Pelabuhan1_idx` (`NamaPelabuhan`),
  ADD KEY `fk_Sesi_Kapal1_idx` (`NamaKapal`);

--
-- Indexes for table `wilayah`
--
ALTER TABLE `wilayah`
  ADD KEY `fk_Wilayah_Pelabuhan1_idx` (`Pelabuhan1`),
  ADD KEY `fk_Wilayah_Pelabuhan2_idx` (`Pelabuhan2`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kapal`
--
ALTER TABLE `kapal`
  ADD CONSTRAINT `fk_Kapal_Pelabuhan` FOREIGN KEY (`Kategori`) REFERENCES `pelabuhan` (`NamaPelabuhan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `nahkoda`
--
ALTER TABLE `nahkoda`
  ADD CONSTRAINT `fk_Nahkoda_Kapal1` FOREIGN KEY (`NamaKapal`) REFERENCES `kapal` (`NamaKapal`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sesi`
--
ALTER TABLE `sesi`
  ADD CONSTRAINT `fk_Sesi_Kapal1` FOREIGN KEY (`NamaKapal`) REFERENCES `kapal` (`NamaKapal`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Sesi_Pelabuhan1` FOREIGN KEY (`NamaPelabuhan`) REFERENCES `pelabuhan` (`NamaPelabuhan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wilayah`
--
ALTER TABLE `wilayah`
  ADD CONSTRAINT `fk_Wilayah_Pelabuhan1` FOREIGN KEY (`Pelabuhan1`) REFERENCES `pelabuhan` (`NamaPelabuhan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Wilayah_Pelabuhan2` FOREIGN KEY (`Pelabuhan2`) REFERENCES `pelabuhan` (`NamaPelabuhan`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
