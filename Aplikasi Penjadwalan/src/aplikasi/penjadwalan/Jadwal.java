package aplikasi.penjadwalan;

import java.util.ArrayList;
import java.util.Collection;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Jadwal {
    ArrayList<Sesi> jadwal;
    ArrayList<Kapal> usedKapals;
    ObservableList<SesiUI> property;

    public Jadwal(){
        jadwal=new ArrayList<Sesi>();
        usedKapals=new ArrayList<Kapal>();
        property = FXCollections.observableArrayList();
    }

    public Sesi findSesi(int sesi, int keberangkatan){
        for (Sesi sesi2 : jadwal) {
            if (sesi2.sesi==sesi && sesi2.keberangkatan==keberangkatan) {
                return sesi2;
            }
        }
        return null;
    }

    public boolean addRelasi(int sesi,int keberangkatan,Kapal kapal) {
        if (usedKapals.contains(kapal)) {
            return true;
        }
        Sesi temp = findSesi(sesi, keberangkatan);
        if (temp == null) {
            temp = new Sesi(sesi,keberangkatan);
            jadwal.add(temp);
        }
        if (temp.kapalCanIn(kapal)) {
            temp.insertKapal(kapal);
            usedKapals.add(kapal);
            System.out.println(kapal);
            property.add(new SesiUI(sesi, keberangkatan, kapal.namaKapal, kapal.namaNahkoda, kapal.kategori.getNama()));
            return true;
        }
        return false;
    }

    public void generateJadwal(Collection<Kapal> kapals, int maksKeberangkatan) {
        for (Kapal kapal : kapals) {
            int sesi = 1;
            int keberangkatan = 1;
            while (!addRelasi(sesi, keberangkatan, kapal)) {
                if (keberangkatan<maksKeberangkatan) {
                    keberangkatan++;
                }
                else{
                    sesi++;
                    keberangkatan=1;
                }
            }
        }
    }

    public void clear() {
        jadwal.clear();
        usedKapals.clear();
        property.clear();
    }
}