/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.penjadwalan;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Boncel
 */
public class DashboardController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private Button btnJadwal;
    @FXML
    private Button btnGenerateJadwal;
    @FXML
    private Button btnKelolaKapal;
    @FXML
    private Button btnKelolaWilayah;
    @FXML
    private Button btnKeluar;
    @FXML
    private Button btnRefreshData;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void kelolaNahkodaAction(ActionEvent event) {
        pane.getScene().setRoot(AplikasiPenjadwalan.genJadwal);
    }

    @FXML
    private void kelolaSesiAction(ActionEvent event) {
        pane.getScene().setRoot(AplikasiPenjadwalan.csp);
    }

    @FXML
    private void kelolaKapalAction(ActionEvent event) {
        pane.getScene().setRoot(AplikasiPenjadwalan.kapal);
    }

    @FXML
    private void kelolaWilayahAction(ActionEvent event) {
        pane.getScene().setRoot(AplikasiPenjadwalan.wilayah);
    }

    @FXML
    private void keluar(ActionEvent event) {
        AplikasiPenjadwalan.stage.close();
        pane.getScene().setRoot(AplikasiPenjadwalan.root);
        AplikasiPenjadwalan.stage.show();
    }

    @FXML
    private void refreshDataAction(ActionEvent event) {
    }
    
}
