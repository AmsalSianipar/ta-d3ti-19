package aplikasi.penjadwalan;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Wilayah {
    FloatProperty Jarak;
    StringProperty Kategori;
    StringProperty Nama;
    public Wilayah(String nama,String kategori,float jarak){
        Jarak = new SimpleFloatProperty(jarak);
        Nama = new SimpleStringProperty(nama);
        Kategori = new SimpleStringProperty(kategori);
    }
}