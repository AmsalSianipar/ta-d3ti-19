package aplikasi.penjadwalan;

import java.util.ArrayList;

public class Sesi {
    static int maksimalKeberangkatan;
    int sesi;
    int keberangkatan;
    ArrayList<Kapal> kapals;

    public Sesi(int sesi,int keberangkatan){
        this.sesi=sesi;
        this.keberangkatan=keberangkatan;
        kapals=new ArrayList<Kapal>();
    }

    public boolean kapalCanIn(Kapal other){
        for (Kapal kapal : kapals) {
            if (kapal.kategori==other.kategori) {
                return false;
            }
        }
        return true;
    }

    public void insertKapal(Kapal kapal) {
        kapals.add(kapal);
    }
}