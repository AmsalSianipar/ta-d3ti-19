package aplikasi.penjadwalan;

import java.util.HashMap;
import java.util.LinkedList;

public class Pelabuhan {
    private String nama;
    HashMap<Pelabuhan,Float> jarak;

    public Pelabuhan() {
        nama= "Pelabuhan";
        jarak= new HashMap<Pelabuhan,Float>();
    }
    public Pelabuhan(String namaPelabuhan) {
        nama= namaPelabuhan;
        jarak= new HashMap<Pelabuhan,Float>();
    }

    public String getNama() {
        return nama;
    }
    public float jarakKe(Pelabuhan lain) {
        return jarak.get(lain);
    }
    public void hubungkan(Pelabuhan lain,float jarak){
        this.jarak.put(lain, jarak);
    }

    public static LinkedList<Pelabuhan> getRute(Pelabuhan awal,Pelabuhan akhir,int banyakPelabuhan) {
        LinkedList<Pelabuhan> rute = new LinkedList<Pelabuhan>();
        LinkedList<Pelabuhan> visited = new LinkedList<Pelabuhan>();
        HashMap<Pelabuhan,Float> distance = new HashMap<Pelabuhan,Float>();
        HashMap<Pelabuhan,LinkedList<Pelabuhan>> steepest = new HashMap<Pelabuhan,LinkedList<Pelabuhan>>();
        Pelabuhan visit = awal;
        steepest.put(awal, new LinkedList<Pelabuhan>());
        distance.put(awal, (Float)(float)0);
        //Find Steepes algorithm
        while (visited.size()<banyakPelabuhan) {
            visited.add(visit);
            for (Pelabuhan wtv : visit.jarak.keySet()) {
                if (distance.containsKey(wtv)) {
                    if (distance.get(wtv)>distance.get(visit)+wtv.jarakKe(visit)) {
                        distance.put(wtv, distance.get(visit)+wtv.jarakKe(visit));
                        steepest.get(wtv).clear();
                        steepest.get(wtv).addAll(steepest.get(visit));
                        steepest.get(wtv).add(visit);
                    }
                } else {
                    distance.put(wtv, visit.jarakKe(wtv));
                    LinkedList<Pelabuhan> val = (LinkedList<Pelabuhan>)steepest.get(visit).clone();
                    val.add(visit);
                    steepest.put(wtv, val);
                }
            }
            boolean full = false;
            for (Pelabuhan pelabuhan : visit.jarak.keySet()){
                if (!full && !visited.contains(pelabuhan)) {
                    visit=pelabuhan;
                    full=true;
                }
            }
        }

        //get real route
        double lowest = Double.MAX_VALUE;
        Pelabuhan lowPelabuhan = null;
        for (Pelabuhan pelabuhan : akhir.jarak.keySet()) {
            if (pelabuhan!=awal && pelabuhan!=akhir) {
                if (pelabuhan.jarakKe(akhir)+distance.get(pelabuhan)<lowest && !steepest.get(pelabuhan).contains(akhir)) {
                    lowPelabuhan=pelabuhan;
                    lowest=pelabuhan.jarakKe(akhir)+distance.get(pelabuhan);
                }
            }
        }
        rute.addAll(steepest.get(akhir));
        rute.add(akhir);
        LinkedList<Pelabuhan> inverse = new LinkedList<Pelabuhan>();
        if (lowPelabuhan!=null) {
            for (Pelabuhan pelabuhan : steepest.get(lowPelabuhan)){
                inverse.addFirst(pelabuhan);
            }
        }
        rute.add(lowPelabuhan);
        rute.addAll(inverse);
        return rute;
    }
}