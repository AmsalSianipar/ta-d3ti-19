/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.penjadwalan;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Boncel
 */
public class LoginController implements Initializable {

    @FXML
    private AnchorPane login;
    @FXML
    private TextField dbname;
    @FXML
    private TextField username;
    @FXML
    private TextField password;
    @FXML
    private TextField server;
    @FXML
    private TextField port;
    @FXML
    private Button buttonlogin;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void loginAction(ActionEvent event) {
        //Preparation for database
        DBHandler dbhandler = DBHandler.getDatabaseHandler();
        try {
            dbhandler.connect(server.getText(), port.getText(), dbname.getText(), username.getText(), password.getText());
            AplikasiPenjadwalan.dashboard = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
            AplikasiPenjadwalan.wilayah=FXMLLoader.load(getClass().getResource("wilayah.fxml"));
            AplikasiPenjadwalan.kapal=FXMLLoader.load(getClass().getResource("kapal.fxml"));
            AplikasiPenjadwalan.genJadwal=FXMLLoader.load(getClass().getResource("steepest.fxml"));
            AplikasiPenjadwalan.csp=FXMLLoader.load(getClass().getResource("csp.fxml"));
            if (AplikasiPenjadwalan.stage!=null) {
                AplikasiPenjadwalan.stage.close();
            }
            login.getScene().setRoot(AplikasiPenjadwalan.dashboard);
            if (AplikasiPenjadwalan.stage!=null) {
                AplikasiPenjadwalan.stage.show();
            }
        } catch (SQLException e) {
            //TODO: handle exception
            System.out.println(e.getMessage());
            Alert a = new Alert(AlertType.ERROR);
            a.setContentText(e.getMessage());
            a.show();
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    @FXML
    private void actionRegister(ActionEvent event) {
    }

    @FXML
    private void kembali(ActionEvent event) {
    }
    
}
