/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.penjadwalan;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Boncel
 */
public class WilayahController implements Initializable {

    @FXML
    private AnchorPane kelolaWilayahPane;
    @FXML
    private TextField jarakField;
    @FXML
    private ComboBox<String> pel1Combo;
    @FXML
    private ComboBox<String> pel2Combo;
    @FXML
    private Button btnTambah;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnHapus;
    @FXML
    private TableView<Wilayah> tblDataRuangan;
    @FXML
    private TableColumn<Wilayah,String> tblKolomNama;
    @FXML
    private TableColumn<Wilayah,Float> tblKolomJarak;
    @FXML
    private TableColumn<Wilayah,String> tblKolomKategori;
    @FXML
    private Button btnDashboard;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tblDataRuangan.setItems(DBHandler.getDatabaseHandler().wilayah);
        tblKolomNama.setCellValueFactory(value->value.getValue().Nama);
        tblKolomJarak.setCellValueFactory(value->value.getValue().Jarak.asObject());
        tblKolomKategori.setCellValueFactory(value->value.getValue().Kategori);
        pel1Combo.setItems(FXCollections.observableArrayList(DBHandler.getDatabaseHandler().pelabuhans.keySet()));
        pel2Combo.setItems(pel1Combo.getItems());
    }    

    @FXML
    private void onClickKategoriCombo(ActionEvent event) {
    }

    @FXML
    private void tambahWilayahAction(ActionEvent event) {
        if (!pel1Combo.getValue().equals(pel2Combo.getValue()) && pel1Combo.getValue()!=null && pel2Combo.getValue()!=null && jarakField.getText()!=null) {
            DBHandler.getDatabaseHandler().add(new Wilayah(pel1Combo.getValue(), pel2Combo.getValue(), Float.parseFloat(jarakField.getText())));
        }
    }

    @FXML
    private void updateWilayahAction(ActionEvent event) {
        if (!pel1Combo.getValue().equals(pel2Combo.getValue()) && pel1Combo.getValue()!=null && pel2Combo.getValue()!=null && jarakField.getText()!=null) {
            DBHandler.getDatabaseHandler().update(tblDataRuangan.getSelectionModel().getSelectedItem(), pel1Combo.getValue(), pel2Combo.getValue(), Float.parseFloat(jarakField.getText()));;
        }
    }

    @FXML
    private void hapusWilayahAction(ActionEvent event) {
        if (tblDataRuangan.getSelectionModel().getSelectedItem()!=null) {
            DBHandler.getDatabaseHandler().delete(tblDataRuangan.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    private void toDashboard(ActionEvent event) {
        kelolaWilayahPane.getScene().setRoot(AplikasiPenjadwalan.dashboard);
    }
    
}
