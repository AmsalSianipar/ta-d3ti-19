package aplikasi.penjadwalan;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Kapal {
    SimpleStringProperty namaKapal;
    SimpleStringProperty perusahaan;
    SimpleIntegerProperty muatan;
    Pelabuhan kategori;
    SimpleStringProperty inisialNahkoda;
    SimpleStringProperty namaNahkoda;

    public Kapal(String namaKapal, String perusahaan,
    int muatan, Pelabuhan kategori, String inisialNahkoda, String namaNahkoda){
        this.namaKapal=new SimpleStringProperty(namaKapal);
        this.perusahaan=new SimpleStringProperty(perusahaan);
        this.muatan=new SimpleIntegerProperty(muatan);
        this.kategori=kategori;
        this.inisialNahkoda=new SimpleStringProperty(inisialNahkoda);
        this.namaNahkoda=new SimpleStringProperty(namaNahkoda);
    }
    
    public String toString() {
        return namaKapal.get();
    }
}