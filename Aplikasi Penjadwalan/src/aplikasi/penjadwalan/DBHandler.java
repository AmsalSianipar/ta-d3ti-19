package aplikasi.penjadwalan;

import java.sql.*;
import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class DBHandler {
    private static DBHandler singleEntity;
    private Connection conn;
    private Statement stmt;
    HashMap<String, Kapal> kapals;
    HashMap<String, Pelabuhan> pelabuhans;
    Jadwal jadwal;
    ObservableList<Wilayah> wilayah;
    ObservableList<Kapal> kapalProperty;

    private DBHandler() {
        wilayah = FXCollections.observableArrayList();
        kapalProperty = FXCollections.observableArrayList();
    }

    public void error(String e) {
        Alert a = new Alert(AlertType.ERROR);
        a.setContentText(e);
        a.show();
    }

    public static DBHandler getDatabaseHandler() {
        if (singleEntity == null) {
            singleEntity = new DBHandler();
        }
        return singleEntity;
    }

    public void connect(String host, String port, String dbname, String username, String password) throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbname
                + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                username, password);
        stmt = conn.createStatement();
        refresh();
    }

    public void refresh() {
        pelabuhans = getPelabuhans();
        kapals = getKapals(pelabuhans);
        kapalProperty.clear();
        kapals.values().forEach(b -> kapalProperty.add(b));
        jadwal = getJadwal(kapals);
    }

    public void add(Kapal k) {
        if (!kapals.containsKey(k.namaKapal.get())) {
            try {
                stmt.executeUpdate(String.format("insert into kapal values (\"%s\",\"%s\",%d,\"%s\")",
                        k.namaKapal.get(), k.perusahaan.get(), k.muatan.get(), k.kategori.getNama()));
                stmt.executeUpdate(String.format("insert into nahkoda values (\"%s\",\"%s\",\"%s\")",
                        k.inisialNahkoda.get(), k.namaNahkoda.get(), k.namaKapal.get()));
                kapals.put(k.namaKapal.get(), k);
                kapalProperty.add(k);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                error(e.getMessage());
            }
        }
    }

    public void update(Kapal kapal, String namaKapal, String perusahaan, int muatan, String kategori, String inisial,
            String namaNahkoda) {
        try {
            stmt.executeUpdate(String.format(
                    "update wilayah set namaKapal=\"%s\",perusahaan=\"%s\",muatan=%d,kategori=\"%s\" where namaKapal=\"%s\"",
                    namaKapal, perusahaan, muatan, kategori, kapal.namaKapal.get()));
            stmt.executeUpdate(String.format(
                    "update nahkoda set inisial=\"%s\",namaNahkoda=\"%s\",namaKapal=\"%s\" where inisial=\"%s\"",
                    inisial, namaNahkoda, namaKapal, kapal.inisialNahkoda));
            kapal.namaKapal.set(namaKapal);
            kapal.perusahaan.set(perusahaan);
            kapal.muatan.set(muatan);
            kapal.kategori = getPelabuhan(kategori);
            kapal.inisialNahkoda.set(inisial);
            kapal.namaNahkoda.set(namaNahkoda);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    public void delete(Kapal w) {
        try {
            stmt.executeUpdate(String.format("delete from nahkoda where inisial=\"%s\"", w.inisialNahkoda.get()));
            stmt.executeUpdate(String.format("delete from kapal where namaKapal=\"%s\"", w.namaKapal.get()));
            kapals.remove(w.namaKapal.get());
            kapalProperty.remove(w);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    public void add(Wilayah w) {
        try {
            stmt.executeUpdate(String.format("insert into wilayah values (\"%s\",\"%s\",%f)", w.Nama.get(),
                    w.Kategori.get(), w.Jarak.get()));
            wilayah.add(w);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    public void update(Wilayah w, String p1, String p2, float jarak) {
        try {
            System.out.println(String.format(
                    "update wilayah set Pelabuhan1=\"%s\",Pelabuhan2=\"%s\",Jarak=%f where Pelabuhan1=\"%s\" and Pelabuhan2=\"%s\")",
                    p1, p2, jarak, w.Nama.get(), w.Kategori.get()));
            stmt.executeUpdate(String.format(
                    "update wilayah set Pelabuhan1=\"%s\",Pelabuhan2=\"%s\",Jarak=%f where Pelabuhan1=\"%s\" and Pelabuhan2=\"%s\"",
                    p1, p2, jarak, w.Nama.get(), w.Kategori.get()));
            w.Nama.set(p1);
            w.Jarak.set(jarak);
            w.Kategori.set(p2);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    public void delete(Wilayah w) {
        try {
            stmt.executeUpdate(String.format("delete from wilayah where Pelabuhan1=\"%s\" and Pelabuhan2=\"%s\"",
                    w.Nama.get(), w.Kategori.get()));
            wilayah.remove(w);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    public Jadwal getJadwal(HashMap<String, Kapal> kapals) {
        try {
            ResultSet jadwalRaw = stmt.executeQuery("select * from Sesi");
            Jadwal temp = new Jadwal();
            while (jadwalRaw.next()) {
                temp.addRelasi(jadwalRaw.getInt("sesi"), jadwalRaw.getInt("keberangkatan"),
                        kapals.get(jadwalRaw.getString("NamaKapal")));
            }
            return temp;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
            return null;
        }
    }

    public void saveJadwal() {
        jadwal.jadwal.forEach(j -> {
            j.kapals.forEach(k -> {
                try {
                    stmt.executeUpdate(String.format("insert into sesi values (%d,%d,\"%s\",\"%s\")", j.sesi,
                            j.keberangkatan, k.namaKapal.get(), k.kategori.getNama()));
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    error(e.getMessage());
                }
            });
        });
    }

    public void clearJadwal() {
        try {
            stmt.executeUpdate("Truncate table sesi");
            jadwal.clear();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    public HashMap<String, Kapal> getKapals(HashMap<String, Pelabuhan> pelabuhans) {
        try {
            ResultSet kapalRaw = stmt.executeQuery("select * from Kapal natural join Nahkoda");
            HashMap<String, Kapal> kapals = new HashMap<String, Kapal>();
            while (kapalRaw.next()) {
                String namaKapal = kapalRaw.getString("NamaKapal");
                kapals.put(namaKapal,
                        new Kapal(namaKapal, kapalRaw.getString("Perusahaan"), kapalRaw.getInt("muatan"),
                                pelabuhans.get(kapalRaw.getString("kategori")), kapalRaw.getString("Inisial"),
                                kapalRaw.getString("namaNahkoda")));
            }
            return kapals;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
            return null;
        }
    }

    Kapal getKapal(String namaKapal){
        return kapals.get(namaKapal);
    }

    Pelabuhan getPelabuhan(String namaPelabuhan){
        return pelabuhans.get(namaPelabuhan);
    }

    public HashMap<String, Pelabuhan> getPelabuhans() {
        try {
            ResultSet pelabuhanRaw = stmt.executeQuery("select * from Pelabuhan");
            HashMap<String, Pelabuhan> pelabuhans = new HashMap<String, Pelabuhan>();
            while (pelabuhanRaw.next()) {
                String tempName = pelabuhanRaw.getString("NamaPelabuhan");
                pelabuhans.put(tempName, new Pelabuhan(tempName));
            }
            ResultSet wilayahRaw = stmt.executeQuery("select * from wilayah");
            while (wilayahRaw.next()) {
                String p1 = wilayahRaw.getString("Pelabuhan1");
                String p2 = wilayahRaw.getString("Pelabuhan2");
                float distance = wilayahRaw.getFloat("Jarak");
                wilayah.add(new Wilayah(p1,p2,distance));
                Pelabuhan pel1 = pelabuhans.get(p1);
                Pelabuhan pel2 = pelabuhans.get(p2);
                pel1.hubungkan(pel2, distance);
                pel2.hubungkan(pel1, distance);
            }
            return pelabuhans;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error(e.getMessage());
            return null;
        }
    }

}