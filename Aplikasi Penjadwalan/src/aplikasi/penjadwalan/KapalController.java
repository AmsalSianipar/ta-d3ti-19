/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.penjadwalan;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Boncel
 */
public class KapalController implements Initializable {

    @FXML
    private AnchorPane kelolaKapalPane;
    @FXML
    private AnchorPane pane;
    @FXML
    private TextField namaField;
    @FXML
    private TextField perusahaanField;
    @FXML
    private TextField muatanField;
    @FXML
    private TextField namaNahkodaField;
    @FXML
    private TextField inisialField;
    @FXML
    private ComboBox<String> kategoriCombo;
    @FXML
    private Button btnTambah;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnHapus;
    @FXML
    private TableView<Kapal> tblDataKapal;
    @FXML
    private TableColumn<Kapal, String> tblKolomNama;
    @FXML
    private TableColumn<Kapal, String> tblKolomPerusahaan;
    @FXML
    private TableColumn<Kapal, Integer> tblKolomMuatan;
    @FXML
    private TableColumn<Kapal, String> tblKolomKategori;
    @FXML
    private TableColumn<Kapal, String> tblKolomNamaNahkoda;
    @FXML
    private TableColumn<Kapal, String> tblKolomInisial;
    @FXML
    private Button btnDashboard;
    @FXML
    private Text totalData;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tblDataKapal.setItems(DBHandler.getDatabaseHandler().kapalProperty);
        tblKolomNama.setCellValueFactory(value->value.getValue().namaKapal);
        tblKolomPerusahaan.setCellValueFactory(value->value.getValue().perusahaan);
        tblKolomMuatan.setCellValueFactory(value->value.getValue().muatan.asObject());
        tblKolomKategori.setCellValueFactory(value->new SimpleStringProperty(value.getValue().kategori.getNama()));
        tblKolomNamaNahkoda.setCellValueFactory(value->value.getValue().namaNahkoda);
        tblKolomInisial.setCellValueFactory(value->value.getValue().inisialNahkoda);
        kategoriCombo.setItems(FXCollections.observableArrayList(DBHandler.getDatabaseHandler().pelabuhans.keySet()));
    }    

    @FXML
    private void onClickKategoriCombo(ActionEvent event) {
    }

    @FXML
    private void tambahAction(ActionEvent event) {
        Kapal k = new Kapal(namaField.getText(), perusahaanField.getText(), Integer.parseInt(muatanField.getText()), DBHandler.getDatabaseHandler().pelabuhans.get(kategoriCombo.getValue()), inisialField.getText(), namaNahkodaField.getText());
        DBHandler.getDatabaseHandler().add(k);
    }

    @FXML
    private void updateAction(ActionEvent event) {
        if (tblDataKapal.getSelectionModel().getSelectedItem()!=null) {
            DBHandler.getDatabaseHandler().update(tblDataKapal.getSelectionModel().getSelectedItem(), namaField.getText(), perusahaanField.getText(), Integer.parseInt(muatanField.getText()), kategoriCombo.getValue(), inisialField.getText(), namaNahkodaField.getText());;
        }
    }

    @FXML
    private void hapusAction(ActionEvent event) {
        if (tblDataKapal.getSelectionModel().getSelectedItem()!=null) {
            DBHandler.getDatabaseHandler().delete(tblDataKapal.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    private void toDashboard(ActionEvent event) {
        pane.getScene().setRoot(AplikasiPenjadwalan.dashboard);
    }
    
}
